//Exceptionnellement outes les classes dans un seul fichier
//pour faciliter la lecture (et éventuellement l'impression)
//Notez que seule la classe DemoRef peut ête "public". Pour les autres on n'indique rien.

import java.util.ArrayList;

public class DemoRedef
{
    public static void main(String[] args)
    {
        Pièce p1 = new Pièce(7, 5);
        System.out.println(p1.superficie()+" m²");//méthode propre à la classe Pièce

        SdB maSalleDeBain = new SdB(2, 3, false, true);
        System.out.println(maSalleDeBain.douche?"Avec douche":"Avec baignoire");//attribut propre à la classe SdB
        System.out.println(maSalleDeBain.longueur);//attribut obtenu par héritage
        System.out.println(maSalleDeBain.superficie()+" m²");//méthode obtenue par héritage

        //p1.superficie() par rapport à maSalleDeBain.superficie()
        // exemple de polymorphisme statique
        System.out.println(p1.toString()+" - "+maSalleDeBain);

        Chambre petiteChambre = new Chambre(3, 4.5, false, false);

        ArrayList<Pièce> maison = new ArrayList<>();
        maison.add(p1);
        maison.add(maSalleDeBain);
        maison.add(petiteChambre);
        System.out.println(maison);

        // exemple de polymorphisme dynamique
        // c'est à l'exécution que Java détermine si on utilise la superficie d'une pièce ou d'une salle de bain
        for ( Pièce p : maison)
        {
            System.out.println(p.toString()+ "->" +p.superficie());
        }

    }
}

class Pièce //extends Object
{
    double longueur, largeur;

    public Pièce(double longueur, double largeur)
    {
        this.longueur = longueur;
        this.largeur = largeur;
    }

    double superficie()
    {
        return longueur*largeur;
    }
}

class SdB extends Pièce //héritage
{
    boolean wc, douche;

    public SdB(double longueur, double largeur, boolean wc, boolean douche) {
        super(longueur, largeur);
        this.wc = wc;
        this.douche = douche;
    }

    double superficie()
    {
        double s = super.superficie();// effectue toujours longueur*largeur
        if ( wc )
            s = s - 0.28;// on enlève 0.28 m² pour tenir compte des WC
        if ( douche )
            s = s - 0.7;
        else
            s = s - 1;
        return s;
    }
}

class Chambre extends Pièce
{
    boolean litDouble, balcon;

    public Chambre(double longueur, double largeur, boolean litDouble, boolean balcon)
    {
        super(longueur, largeur);
        this.litDouble = litDouble;
        this.balcon = balcon;
    }
}
