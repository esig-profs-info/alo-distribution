/**
 * March 2020
 *
 * @author Mirko Steinle
 */
public class Forme
{

    private String couleur;

    /**
     * Position de la Forme sur l'axe x (en nombre de pixels)
     */
    private int x;
    /**
     * Position de la Forme sur l'axe y (en nombre de pixels)
     */
    private int y;

    public Forme(String couleur, int x, int y)
    {
        this.couleur = couleur;
        this.x = x;
        this.y = y;
    }

    public String toString()
    {
        return " de couleur " + couleur + " à la position (" + x + ", " + y + ")";
    }
}
