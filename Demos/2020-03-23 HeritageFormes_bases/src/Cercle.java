/**
 * March 2020
 *
 * @author Mirko Steinle
 */
public class Cercle extends Forme
{
    private double rayon;

    public double getRayon()
    {
        return rayon;
    }

    public Cercle(double rayon, String couleur)
    {
        super(couleur, 0, 0);
        this.rayon = rayon;
    }

    @Override
    public String toString()
    {
        return "Cercle avec rayon " + rayon + super.toString();
    }
}
