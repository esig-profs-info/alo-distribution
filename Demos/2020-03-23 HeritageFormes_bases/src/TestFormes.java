import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * March 2020
 *
 * @author Mirko Steinle, Thomas Servettaz, Bruno Pereira
 */
public class TestFormes
{
    public static void main(String[] args)
    {
       /* Cercle c1 = new Cercle(2.5, "bleu");
        System.out.println(c1);

        Rectangle r1 = new Rectangle(2, 3, "jaune");
        System.out.println(r1);*/

        ArrayList<Forme> lesFormes = lireDonnees("forme-cerclesrectangles.txt");

        //afficher les formes:
        afficheFormes(lesFormes);
    }


    private static ArrayList<Forme> lireDonnees(String fileUrl) {
        ArrayList<Forme> listeDonnees = new ArrayList<Forme>();
        try {
            Scanner scanSurFichier = new Scanner(new File(fileUrl));
            while(scanSurFichier.hasNextLine()){
                listeDonnees.add(lireLigne(scanSurFichier.nextLine()));
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return listeDonnees;
    }

    private static Forme lireLigne(String ligne)
    {
        Scanner scanSurLigne = new Scanner(ligne);
        String forme = scanSurLigne.next();
        if (forme.equals("rectangle")) {
            return lireRectangle(scanSurLigne);
        }
        else {
            return lireCercle(scanSurLigne);
        }
        // return "rectangle".equals(forme) ? lireRectangle(scanSurLigne) : lireCercle(scanSurLigne);
    }

    private static Rectangle lireRectangle(Scanner scanner)
    {
        String couleur = scanner.next();
        double a = scanner.nextDouble();
        double b = scanner.nextDouble();
        return new Rectangle(a, b, couleur);
    }

    private static Cercle lireCercle(Scanner scanner)
    {
        String couleur = scanner.next();
        int rayon = scanner.nextInt();
        return new Cercle(rayon, couleur);
    }

    private static void afficheFormes(ArrayList<Forme> lesFormes) {
        for (Forme element : lesFormes)
        {
            System.out.println(element);
        }
    }

    /*private static void afficheCercles(ArrayList<Cercle> lesCercles) {
        for (Cercle element : lesCercles)
        {
            System.out.println(element);
        }
    }*/
}