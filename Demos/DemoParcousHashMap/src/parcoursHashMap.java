import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class parcoursHashMap // première erreur : ne respecte pas la convention d'une initiale majuscule pour un nom de classe
{

    public static void main(String[] args)
    {
        HashMap<String,Double> map = new HashMap<String,Double>(); // la répétition de String,Double est facultative,
                                                                   // ce n'est pas une erreur

        map.put("A",12.0);
        map.put("B",42.1);
        map.put("C",5.6);
        map.put("D",29.7);

        //Boucle for
        System.out.println("Boucle for:");
        for (Map.Entry mapentry : map.entrySet()) // les types génériques devraient être précisés après Map.Entry
        {
            System.out.println("clé: "+mapentry.getKey()
                    + " | valeur: " + mapentry.getValue());
            // la ligne ci-dessous est un ajout pour montrer les problèmes de l'oubli des types génériques
            // length est rouge : cannot resolve methode 'length' in 'Object'
            // mapentry.getValue()*2 est souligné en rouge : Operator '*' cannot be applied to 'java.lang.Object', 'int'
//            System.out.println( "longueur de la clé: "+mapentry.getKey().length()
//                                + " | double de la valeur: " + mapentry.getValue()*2);
            // notez enfin - un petit détail de présentation qu'il n'y a pas d'espace autur du 1er + alors qu'il y en a
            // autour du dernier. Mais ce n'est pas une erreur.
        }

        //Boucle while+iterator
        // une autre façon de parcourir
        System.out.println("Boucle while");
        Iterator iterator = map.entrySet().iterator();
        while (iterator.hasNext())
        {
            Map.Entry mapentry = (Map.Entry) iterator.next();// même problème
            System.out.println("clé: "+mapentry.getKey()
                    + " | valeur: " + mapentry.getValue());
//            System.out.println( "longueur de la clé: "+mapentry.getKey().length()
//                                + " | double de la valeur: " + mapentry.getValue()*2);

        }
    }
}