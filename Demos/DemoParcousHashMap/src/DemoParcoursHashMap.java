// ce projet contient deux versions de la même classe
// l'une est reprise directement d'un site Web http://www.codeurjava.com/2015/06/comment-parcourir-une-hashmap-en-java.html
// l'autre est la version corrigée, elle est donnée ci-dessuos car on ne peut avoir deux fichiers dans un même projet
// dont les noms diffèrent par une simple distinction minuscule/majuscule

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class DemoParcoursHashMap
{
    public static void main(String[] args)
    {
        //version imparfaite
        parcoursHashMap.main(null);

        //version corrigée
        ParcoursHashMapCorrigé.main(null);
    }
}

class ParcoursHashMapCorrigé
{
    public static void main(String[] args)
    {
        HashMap<String,Double> map = new HashMap<>();

        map.put("A", 12.0);
        map.put("B", 42.1);
        map.put("C", 5.6);
        map.put("D", 29.7);

        //Boucle for
        System.out.println("Boucle for:");
        for (Map.Entry<String,Double> mapentry : map.entrySet()) // les types génériques devraient être précisés après Map.Entry
        {
            System.out.println("clé: "+mapentry.getKey()
                    + " | valeur: " + mapentry.getValue());
            System.out.println( "longueur de la clé: "+mapentry.getKey().length()
                                + " | double de la valeur: "+mapentry.getValue()*2);
        }

        //Boucle while+iterator
        // une autre façon de parcourir
        System.out.println("Boucle while");
        Iterator iterator = map.entrySet().iterator();
        while (iterator.hasNext())
        {
            Map.Entry<String,Double> mapentry = (Map.Entry) iterator.next();// même problème
            System.out.println("clé: "+mapentry.getKey()
                    + " | valeur: " + mapentry.getValue());
            System.out.println( "longueur de la clé: "+mapentry.getKey().length()
                                + " | double de la valeur: " + mapentry.getValue()*2);

        }
    }
}


