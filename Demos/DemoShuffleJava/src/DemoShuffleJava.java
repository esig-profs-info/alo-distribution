import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class DemoShuffleJava
{
    public static void main(String[] args)
    {
        ArrayList<Integer> listeDépart = new ArrayList<>();
        for (int i = 0; i < 50; i++)
        {
            listeDépart.add(i);
        }
        // vérifions que la iste a bien 50 nombres dans l'ordre
        System.out.println(listeDépart);

        //appliquons Shuffle pour tout mélanger
        //ATTENTION ! Cela modifie la liste donnée en paramètre !
        Collections.shuffle(listeDépart);

        //on prend les 10 premiers éléments, tous dans la liste de départ et tous différents par construction
        System.out.println(listeDépart.subList(0, 10));
        // 0 est l'indice de départ et mais 10 représente un de plus que l'indice de fin
    }
}
