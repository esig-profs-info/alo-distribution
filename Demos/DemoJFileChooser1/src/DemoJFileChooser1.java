import javax.swing.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class DemoJFileChooser1
{
    public static void main(String[] args)
    {
        //démoDeBase();// boîte de dialogue d'ouverture d'un fichier
        //démoTestTousLesCas();//APPROVE et CANCEL sont les deux vraiment utiles en pratique
        //démoSauvegarde(); // boîte de dialogue d'enregistrement d'un fichier
        //démoAChoix("MonMessage"); // boîte de dialogue avec message personnalisé
        //démoAvecAffichage(); // ouverture et affichage d'un fichier texte
    }

    private static void démoAvecAffichage()
    // cette version reprend la démo de base
    // et y ajoute l'affichage d'un fichier texte (extension .txt ou .java) dans le log
    {
        JFileChooser jfc = new JFileChooser();

        if ( jfc.showOpenDialog(null) == JFileChooser.APPROVE_OPTION )
        {
            File fichierChoisi = jfc.getSelectedFile();
            System.out.println("la spécification du fichier "+fichierChoisi);
            String nomFichierChoisi = fichierChoisi.getName();
            System.out.println("juste le nom du fichier "+nomFichierChoisi);
            if ( nomFichierChoisi.endsWith("txt") || nomFichierChoisi.endsWith("java") )
            {
                System.out.println("C'est sans doute un fichier texte. Voici son contenu");
                afficherFichier(fichierChoisi);
                // NB : on passe le File plutôt que la String
                // pour éviter un new File(nomFichierChoisi) dans afficherFichier
            }
        }
    }


    private static void démoAChoix(String monMessage)
    {
        JFileChooser jfc = new JFileChooser();

        if ( jfc.showDialog(null, monMessage) == JFileChooser.APPROVE_OPTION )
        {
            System.out.println("la spécification du fichier "+jfc.getSelectedFile());
            System.out.println("juste le nom du fichier "+jfc.getSelectedFile().getName());
        }
    }

    private static void afficherFichier(File fichierChoisi)
    {
        try
        {
            Scanner flux = new Scanner(fichierChoisi);
            while ( flux.hasNext() )
                System.out.println(flux.nextLine());
        }
        catch (FileNotFoundException fnfe)
        {
            fnfe.printStackTrace();
        }
    }


    private static void démoSauvegarde()
    {
        JFileChooser jfc = new JFileChooser();

        if ( jfc.showSaveDialog(null) == JFileChooser.APPROVE_OPTION )
        {
            System.out.println("la spécification du fichier "+jfc.getSelectedFile());
            System.out.println("juste le nom du fichier "+jfc.getSelectedFile().getName());
        }
    }


    private static void démoTestTousLesCas()
    {
        JFileChooser jfc = new JFileChooser();

        int codeRéponse = jfc.showOpenDialog(null);
        if (  codeRéponse == JFileChooser.APPROVE_OPTION )
        {
            System.out.println("la spécification du fichier "+jfc.getSelectedFile());
            System.out.println("juste le nom du fichier "+jfc.getSelectedFile().getName());
        }
        else if ( codeRéponse == JFileChooser.CANCEL_OPTION )
            System.out.println("L'utilisateur a annulé");
        else if ( codeRéponse == JFileChooser.ERROR_OPTION )//je ne vois pas comment l'obtenir !
            System.out.println("Erreur");
        else
            System.out.println("Impossible");
    }


    private static void démoDeBase()
    {
        JFileChooser jfc = new JFileChooser();

        if ( jfc.showOpenDialog(null) == JFileChooser.APPROVE_OPTION )
        {
            System.out.println("la spécification du fichier "+jfc.getSelectedFile());
            System.out.println("juste le nom du fichier "+jfc.getSelectedFile().getName());
        }
    }
}
