import java.util.HashMap;
import java.util.Map;

public class Main {

    public static void main(String[] args) {
        HashMap<String, Integer> score = new HashMap<>();
        score.put("Dagobert", 34);
        score.put("Minnie", 54);

        System.out.println("Score de Dagobert: " + score.get("Dagobert"));
        score.put("Dagobert", 10);
        System.out.println("Score de Dagobert: " + score.get("Dagobert"));

        System.out.println("Mickey joue: " + (score.containsKey("Mickey")? "oui" : "non"));
        System.out.println("Minnie joue: " + score.containsKey("Minnie"));

        for (Map.Entry<String, Integer> scoreIndividuel : score.entrySet())
        {
            if (scoreIndividuel.getValue() > 40)
            {
                System.out.println(scoreIndividuel.getKey());
            }
        }
        System.out.println("Tous les joueurs:");
        System.out.println(score.keySet());
        for (String s : score.keySet())
        {
            System.out.println(s);
        }

    }
}
