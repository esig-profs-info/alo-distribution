import java.util.ArrayList;
import java.util.HashMap;

public class Run
{
    public static void main(String[] args)
    {
        HashMap<String, Integer> dico = new HashMap<>();
        dico.put("Dagobert", 34);
        dico.put("Minnie", 54);
        dico.put("Dingo", 34);//pour avoir deux clés ayant la même valeur
        System.out.println(dico);

        System.out.println("version 1 (lourde) ");
        System.out.println(inv1(dico));
        System.out.println();

        System.out.println("version 2 (légère) ");  
        System.out.println(inv2(dico));
        System.out.println();

        System.out.println("version générique ");
        HashMap<Integer, ArrayList<String>> revScore = invGenerique(dico);
        System.out.println(revScore);
        System.out.println();
    }

    static HashMap<Integer, ArrayList<String>> inv1(HashMap<String, Integer> dico)
    {
        HashMap<Integer, ArrayList<String>> revDico = new HashMap<>();

        for ( Integer valeurEntière : dico.values() )
        {
            if ( !revDico.containsKey(valeurEntière) )
                revDico.put(valeurEntière, new ArrayList<String>());    
            for ( String clé : dico.keySet() )
                if ( dico.get(clé).equals(valeurEntière) && !revDico.get(valeurEntière).contains(clé))
                    revDico.get(valeurEntière).add(clé);
        }
        return revDico;
    }

    static HashMap<Integer, ArrayList<String>> inv2(HashMap<String, Integer> dico)
    {
        HashMap<Integer, ArrayList<String>> revDico = new HashMap<>();

        for ( String clé : dico.keySet() )
        {
           Integer valeurEntière = dico.get(clé);
           if ( !revDico.containsKey(valeurEntière) )
               revDico.put(valeurEntière, new ArrayList<String>());
           revDico.get(valeurEntière).add(clé);
        }
        return revDico;
    }

    static <K, V> HashMap<V, ArrayList<K>> invGenerique(HashMap<K, V> dico)
    {
        HashMap<V, ArrayList<K>> revDico = new HashMap<>();

        for ( K clé : dico.keySet() )
        {
            V valeurEntière = dico.get(clé);
            if ( !revDico.containsKey(valeurEntière) )
                revDico.put(valeurEntière, new ArrayList<K>());
            revDico.get(valeurEntière).add(clé);
        }
        return revDico;
    }
}
