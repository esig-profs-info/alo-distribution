import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * March 2020
 *
 * @author Mirko Steinle, Thomas Servettaz, Bruno Pereira
 */
public class TestFormes
{
    public static void main(String[] args)
    {

        ArrayList<Forme> lesFormes = lireDonnees("forme-cerclesrectangles.txt");

        afficheFormes(lesFormes);
    }


    private static ArrayList<Forme> lireDonnees(String fileUrl) {
        ArrayList<Forme> liste = new ArrayList<Forme>();
        try {
            Scanner scanFichier = new Scanner (new File(fileUrl));
            while (scanFichier.hasNextLine() ) {
                String nvlleLigne = scanFichier.nextLine();
                Scanner scanLigne = new Scanner(nvlleLigne);
                String type = scanLigne.next();
                if (type.equals("cercle") /* scanLigne.next().equals("cercle") */){
                    String couleur = scanLigne.next();
                    int rayon = scanLigne.nextInt();
                    liste.add(new Cercle(rayon, couleur));
                }
                else if (type.equals("rectangle")
                    /* Problème avec scanLigne.next().equals("rectangle"):
                        Le type a déjà été consumé/lu précédemment, càd le curseur a avancé à la couleur et ce n'est donc jamais égal à "rectangle"
                    */) {
                    String couleur = scanLigne.next();
                    double a = scanLigne.nextDouble();
                    double b = scanLigne.nextDouble();
                    liste.add(new Rectangle(a, b, couleur));
                } else if(type.equals("triangle")) {
                    // ...
                } else {
                    System.err.println("Forme inconnue: " + type);
                }

//                // Version avec switch:
//                String couleur = scanLigne.next();
//                switch (type)
//                {
//                    case "cercle":
//                        int rayon = scanLigne.nextInt();
//                        liste.add(new Cercle(rayon, couleur));
//                        break;
//                    case "rectangle":
//                        double a = scanLigne.nextDouble();
//                        double b = scanLigne.nextDouble();
//                        liste.add(new Rectangle(a, b, couleur));
//                        break;
//                    case "triangle":
//                        // ...
//                        break;
//                    default:
//                        System.err.println("Forme inconnue: " + type);
//                }

            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return liste;
    }


    private static void afficheFormes(ArrayList<Forme> lesFormes) {
        for (Forme element : lesFormes)
        {
            System.out.println(element);
        }
    }

    /*private static void afficheCercles(ArrayList<Cercle> lesCercles) {
        for (Cercle element : lesCercles)
        {
            System.out.println(element);
        }
    }*/
}