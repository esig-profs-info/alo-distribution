/**
 * March 2020
 *
 * @author Mirko Steinle
 */
public class Rectangle extends Forme
{
    private double a;
    private double b;

    public Rectangle(double a, double b, String couleur)
    {
        super(couleur, 0, 0);
        this.a = a;
        this.b = b;
    }

    @Override
    public String toString()
    {
        return "Rectangle avec a " + a + " b " + b + super.toString();
    }
}
