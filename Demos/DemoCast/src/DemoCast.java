import java.util.ArrayList;

public class DemoCast
{
    public static void main(String[] args)
    {
// écriture à éviter car création inutile d'une liste vide
//        ArrayList<String> listeCommissions = new ArrayList<>();
//        listeCommissions = lireDonnées("DataDeMaTata.txt");
        ArrayList<String> listeCommissions = lireDonnées("DataDeMaTata.txt");

        ArrayList<Forme> listeFormes = new ArrayList<>();
        listeFormes.add(new Forme("toute bête"));
        listeFormes.add(new Rectangle("un carré", 5, 5));
        listeFormes.add(new Cercle("Rond", 6));
        exempleInstanceOf(listeFormes);

        exempleAccèsAttributSuperCLasse(listeFormes);
        System.out.println(new Cercle("démo", 4).rayon);//juste pour la démo
        if ( listeFormes.get(2) instanceof Cercle )
        {
            System.out.println("C'est un cercle !");
            //System.out.println(listeFormes.get(2).rayon); //impossible rayon inaccessible
            Cercle unCercle = (Cercle)(listeFormes.get(2)); // transtypage
            System.out.println(unCercle.rayon);

            //IntelliJI insère un transtypage automatique si on tape point et on accepte la suggestion rayon
            //double rayonCercle =  listeFormes.get(2)

            //autre écriture moins lisible
            System.out.println(((Cercle) listeFormes.get(2)).rayon);

            //exemple erreur de transtypage - syntaxe acceptée mais erreur d'exécution
            System.out.println((Cercle)listeFormes.get(1));// le deuxième est un Rectangle

            //Cercle f = listeFormes.get(1); // syntaxe refusée

            //autre exemple de transtypage
            int codeDeA = (int)'A'; //caractère unique entre apostrophes
            char espace = (char)32;
            System.out.println("code de A : "+codeDeA+" et un espace :_"+espace+"_");
        }

    }

    private static void exempleAccèsAttributSuperCLasse(ArrayList<Forme> listeFormes)
    {
        for ( Forme f : listeFormes )
        {
            System.out.println(f.nom);
        }
    }

    public static void exempleInstanceOf(ArrayList<Forme> listeFormes)
    {
        for ( Forme f : listeFormes )
            if ( f instanceof Rectangle )
                System.out.println("Rectangle");
            else if ( f instanceof Cercle )
                System.out.println("Cercle");
            else //
                System.out.println("Une Forme qui n'est ni un Cercle, ni un Rectangle");
    }



    public static ArrayList<String> lireDonnées(String nomFichier)
    {
        return null;
    }

}

/*abstract*/ class Forme
{
    String nom;

    public Forme(String nom)
    {
        this.nom = nom;
    }
}

class Rectangle extends Forme
{
    double longueur, largeur;

    public Rectangle(String nom, double longueur, double largeur)
    {
        super(nom);
        this.longueur = longueur;
        this.largeur = largeur;
    }
}

class Cercle extends Forme
{
    double rayon;

    public Cercle(String nom, double rayon)
    {
        super(nom);
        this.rayon = rayon;
    }
}
