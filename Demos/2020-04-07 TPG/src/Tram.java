/**
 * April 2020
 *
 * @author Mirko Steinle
 */
public class Tram extends Vehicule
{
    private String marque;
    private int nbWagons;

    public String getDirectionalite()
    {
        return "Duewag-Vevey".equals(marque) ? "monodirectionnel" : "bidirectionnel";
    }

    Tram(int nbPassagersMax, long prixAchat, String marque, int nbWagons)
    {
        super(nbPassagersMax, prixAchat);
        this.marque = marque;
        this.nbWagons = nbWagons;
    }

    @Override
    public String toString()
    {
        return super.toString() + String.format("Tramway (%s) composé de %d wagons", getDirectionalite(), nbWagons);
    }



}
