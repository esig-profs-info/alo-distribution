/**
 * April 2020
 *
 * @author Mirko Steinle
 */
public class Mouette extends Vehicule
{

    private int nbGilets;

    public int getNbGilets()
    {
        return nbGilets;
    }

    void setNbGilets(int nbGilets)
    {
        verifierCoherence(nbGilets, getNbPassagersMax());
        this.nbGilets = nbGilets;
    }

    void setNbPassagersMax(int nbPassagersMax)
    {
        verifierCoherence(nbGilets, nbPassagersMax);
        super.setNbPassagersMax(nbPassagersMax);
    }

    private void verifierCoherence(int nbGilets, int nbPassagersMax)
    {
        if(nbGilets < nbPassagersMax + 1)
        {
            //throw new IllegalArgumentException("Nb de gilets insuffisants. Gilets: " + nbGilets + ", nbPassagersMax: " + nbPassagersMax);
            System.err.println("Nb de gilets insuffisants. Gilets: " + nbGilets + ", nbPassagersMax: " + nbPassagersMax);
        }
    }

    Mouette(int nbPassagersMax, long prixAchat, int nbGilets)
    {
        super(nbPassagersMax, prixAchat);
        setNbGilets(nbGilets);
    }

    private boolean assezDeGilets()
    {
        return nbGilets > getNbPassagersMax();
    }

    @Override
    public String toString()
    {
        String gilets = assezDeGilets() ? "" : " (PAS ASSEZ DE GILETS!)";
        return super.toString() + String.format("Mouette avec %d gilets de secours%s", nbGilets, gilets);
    }



}
