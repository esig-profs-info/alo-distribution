/**
 * April 2020
 *
 * @author Mirko Steinle
 */
public class TestVehicules
{
    public static void main(String[] args)
    {
        Mouette m1 = new Mouette(50, 80000, 50);
        Mouette m2 = new Mouette(60, 85000, 61);
        Vehicule[] vehicules = {
                m1, m2,
                new Tram(90, 640000, "Bombardier", 6),
                new Tram(89, 430000, "Duewag-Vevey", 4)
        };
        for (Vehicule vehicule : vehicules)
        {
            System.out.println(vehicule);
        }
        m1.setNbGilets(100);
        m1.setNbGilets(0);
        m2.setNbGilets(10);
        m2.setNbGilets(65);

        m2.setNbPassagersMax(65);
        System.out.println(m2);
    }
}
