/**
 * April 2020
 *
 * @author Mirko Steinle
 */
public class Vehicule
{
    private int nbPassagersMax;
    private long prixAchat;

    int getNbPassagersMax()
    {
        return nbPassagersMax;
    }

    void setNbPassagersMax(int nbPassagersMax)
    {
        this.nbPassagersMax = nbPassagersMax;
    }

    Vehicule(int nbPassagersMax, long prixAchat)
    {
        this.nbPassagersMax = nbPassagersMax;
        this.prixAchat = prixAchat;
    }

    @Override
    public String toString()
    {
        return String.format("Véhicule (prix: CHF %d, nb. passagers max: %d) de type: ", prixAchat, nbPassagersMax);
    }
}
