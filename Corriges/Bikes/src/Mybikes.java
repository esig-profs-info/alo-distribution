import java.io.File;
import java.io.FileNotFoundException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * @(#)Mybikes.java
 *
 * @author Thomas Servettaz
 */

public class Mybikes {

	public static void main(String[]args){
		ArrayList<Reparation> listeRep = lireDonnees("reparations.txt");
		/*System.out.println(listeRep[4].calculerPrix(120));
		System.out.println("blabla");*/
		//afficherReparations(listeRep);
		//afficherMontants(listeRep);
		afficherMoyennes(listeRep);
	}

	private static void afficherReparations(ArrayList<Reparation> listeRep) {
		int cpt=1;
		for(Reparation r:listeRep){
			System.out.println("reparation n°"+cpt+" "+r);
			cpt++;
		}
	}

	private static void afficherMontants(ArrayList<Reparation> listeRep) {
		int cpt=1;
		for(Reparation r:listeRep){
			System.out.println("Montant de la reparation n°"+cpt+" "+r.calculerPrix(120));
			cpt++;
		}
	}

	private static void afficherMoyennes(ArrayList<Reparation> listeRep){
		int sommeDureePayees = 0;
		int sommeDureeOuvertes = 0;
		int cptOuvertes = 0;
		int cptPayees = 0;
		double moyenneOuvertes;
		double moyennePayees;

		for(Reparation r:listeRep){
			if("ouverte".equals(r.getStatut())){
				sommeDureeOuvertes += r.getDureeEnMinute();
				cptOuvertes++;
			}
			else{
				sommeDureePayees += r.getDureeEnMinute();
				cptPayees++;
			}
		}
		if(cptOuvertes != 0)
			moyenneOuvertes = sommeDureeOuvertes * 1.0 / cptOuvertes;
		else
			moyenneOuvertes = 0;
		if(cptPayees != 0)
			moyennePayees = sommeDureePayees * 1.0 / cptPayees;
		else
			moyennePayees = 0;

		System.out.println("moyenne des durées pour les "+cptOuvertes+" réparation(s) ouvertes: "+moyenneOuvertes+" min.");
		System.out.println("moyenne des durées pour les "+cptPayees+" réparation(s) payées: "+moyennePayees+" min.");
	}

	public static ArrayList<Reparation> lireDonnees(String nomFichier){
		//Reparation[] tabAretourner  = new Reparation[10];
		ArrayList<Reparation> listeAretourner = new ArrayList<Reparation>();
		try {
			Scanner scanner = new Scanner(new File(nomFichier));
			while(scanner.hasNextLine()){
				String ligne = scanner.nextLine();
				Scanner scanRep  =  new Scanner(ligne);
				LocalDate date = trouverDateDepuisString(scanRep.next());
				int duree = scanRep.nextInt();
				String description = scanRep.next();
				String statut = scanRep.next();
				Reparation r = new Reparation(description);
				r.setDate(date);
				r.setDureeEnMinute(duree);
				r.setStatut(statut);
				listeAretourner.add(r);
			}
		}
		catch(FileNotFoundException fnfe){
			System.out.println("fichier non trouvé");
		}
		return listeAretourner;
	}

	public static LocalDate trouverDateDepuisString(String s){
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		return LocalDate.parse(s,dtf);
	}
}