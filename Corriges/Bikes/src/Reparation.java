import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * @(#)Reparation.java
 *
 *
 * @author Thomas Servettaz
 * @version 1.00 2017/3/1
 */


public class Reparation {
    private LocalDate date;
    private int dureeEnMinute;
    private String description;
    private String statut;

    public Reparation(String description) {
        this.description = description;
        statut = "ouverte";
    }

    public LocalDate getDate() {
        return date;
    }

    public double calculerPrix(double tarifHoraire){
        return dureeEnMinute*1.0/60 * tarifHoraire;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public int getDureeEnMinute() {
        return dureeEnMinute;
    }

    public void setDureeEnMinute(int dureeEnMinute) {
        this.dureeEnMinute = dureeEnMinute;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatut() {
        return statut;
    }

    public void setStatut(String statut) {
        this.statut = statut;
    }

    @Override
    public String toString() {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/YYYY");
        String dateString = date.format(dtf);
        return "Reparation ("+statut+") du "+dateString+" --> "+description+" ("+dureeEnMinute+" min.)";
    }
}