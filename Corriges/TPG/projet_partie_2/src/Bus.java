import java.time.LocalDate;

/**
 * April 2020
 *
 * @author Thomas Servettaz
 */
public class Bus extends Vehicule{
    private double consommation; //consommation de diesel pour 100km

    public void setConsommation(double consommation) {
        this.consommation = consommation;
    }


    public Bus(int nbPassagersMax, long prixAchat, LocalDate dateAcquisition, double consommation, int matricule, String marque) {
        super(nbPassagersMax, prixAchat, dateAcquisition, matricule, marque);
        setConsommation(consommation);
    }
}
