import java.time.LocalDate;
import java.util.ArrayList;

/**
 * April 2020
 *
 * @author Mirko Steinle, Thomas Servettaz
 */
public class Vehicule
{
    private int nbPassagersMax;
    private long prixAchat;
    private int kmAuCompteur;
    private LocalDate dateAcquisition;
    private ArrayList<String> listeLignes;
    private int matricule;
    private String marque;

    public void setKmAuCompteur(int kmAuCompteur) {
        if(kmAuCompteur > 0)
            this.kmAuCompteur = kmAuCompteur;
        else
            throw new IllegalArgumentException();
    }

    public void setListeLignes(ArrayList<String> listeLignes) {
        this.listeLignes = listeLignes;
    }

    public int getNbPassagersMax()
    {
        return nbPassagersMax;
    }

    public void setNbPassagersMax(int nbPassagersMax)
    {
        this.nbPassagersMax = nbPassagersMax;
    }

    public Vehicule(int nbPassagersMax, long prixAchat, LocalDate dateAcquisition, int matricule, String marque)
    {
        this.nbPassagersMax = nbPassagersMax;
        this.prixAchat = prixAchat;
        this.dateAcquisition = dateAcquisition;
        this.matricule = matricule;
        this.marque = marque;
    }

    public ArrayList<String> getListeLignes() {
        return listeLignes;
    }

    @Override
    public String toString()
    {
        return String.format("Véhicule (prix: CHF %d, nb. passagers max: %d) de type: ", prixAchat, nbPassagersMax);
    }
}