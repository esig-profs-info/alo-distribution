import java.util.ArrayList;

/**
 * April 2020
 *
 * @author Thomas Servettaz
 */
public class Ligne {
    private String nom;
    private ArrayList<String> listeDesArrets;
    private ArrayList<Integer> listDureeEntreArrets;
    private boolean compatibleTrolley;
    private int tensionLigne;

    Ligne(String nom)
    {
        this.nom = nom;
        listeDesArrets = new ArrayList<>();
        listDureeEntreArrets = new ArrayList<>();
        compatibleTrolley = false;
        tensionLigne = -1;
    }

    public String getNom() {
        return nom;
    }

    public void setCompatibleTrolley(boolean compatibleTrolley)
    {
        this.compatibleTrolley = compatibleTrolley;
    }

    public void setTensionLigne(int tensionLigne)
    {
        this.tensionLigne = tensionLigne;
    }

    boolean hasNom(String nom)
    {
        return this.nom.equals(nom);
    }

    void ajouterArret(String arret)
    {
        listeDesArrets.add(arret);
    }

    void ajouterMinutage(int minutage)
    {
        listDureeEntreArrets.add(minutage);
    }

    @Override
    public String toString() {
        return "Ligne{" +
                "nom='" + nom + '\'' +
                ", compatibleTrolley=" + compatibleTrolley +
                ", tensionLigne=" + tensionLigne +
                '}';
    }
}
