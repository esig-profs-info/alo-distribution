import java.io.File;
import java.io.FileNotFoundException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * May 2020
 *
 * @author Mirko Steinle
 */
public class LectureVehicules
{
    ArrayList<Vehicule> lireVehicules(String fileName) {
        ArrayList<Vehicule> listeVehicules = new ArrayList<Vehicule>();
        try (Scanner scanSurFichier = new Scanner(new File(fileName))) {
            scanSurFichier.nextLine(); // on saute la ligne des titres
            while (scanSurFichier.hasNextLine()){
                String ligne = scanSurFichier.nextLine();
                Scanner scanSurLigne = new Scanner(ligne);
                scanSurLigne.useDelimiter(",");
                listeVehicules.add(lireLigne(scanSurLigne));
            }
        }
        catch(FileNotFoundException fnfe){
            System.out.println("fichier non trouvé!");
        }
        return listeVehicules;
    }

    private Vehicule lireLigne(Scanner scanSurLigne){
        int matricule = scanSurLigne.nextInt();
        String type = scanSurLigne.next();
        String marque = scanSurLigne.next();
        int prixAchat = scanSurLigne.nextInt();
        int nbPassagers = scanSurLigne.nextInt();
        LocalDate dateAcquisition = creerDate(scanSurLigne.next());
        int km = scanSurLigne.nextInt();
        ArrayList<String> listeLignes = creerListeLignes(scanSurLigne.next());

        if("trolley".equals(type)){
            int tensionNecessaire = scanSurLigne.nextInt();
            boolean moteurSecours = "oui".equals(scanSurLigne.next());
            Trolley t = new Trolley(nbPassagers,prixAchat,dateAcquisition,tensionNecessaire,moteurSecours, matricule, marque);
            t.setKmAuCompteur(km);
            t.setListeLignes(listeLignes);
            return t;
        }
        else if("autobus".equals(type)){
            double consommation = scanSurLigne.nextDouble();
            Bus b = new Bus(nbPassagers,prixAchat,dateAcquisition, consommation, matricule, marque);
            b.setKmAuCompteur(km);
            b.setListeLignes(listeLignes);
            return b;
        }
        System.out.println("type de vehicule non reconnu");
        return null;
    }

    private ArrayList<String> creerListeLignes(String lignesEnString) {
        ArrayList<String> listeLignes = new ArrayList<>();
        String[] lignes_arr = lignesEnString.split("-");
        for (String ligne: lignes_arr) {
            listeLignes.add(ligne);
        }
        return listeLignes;

        //ou, en plus court:
        // return new ArrayList<String>(Arrays.asList(lignesEnString.split("-")));
    }



    private LocalDate creerDate(String dateEnString) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        return LocalDate.parse(dateEnString, formatter);
    }
}
