import sun.awt.image.IntegerComponentRaster;

import javax.lang.model.element.VariableElement;
import java.util.ArrayList;
import java.util.HashMap;

public class TestLignes {

    public static void main(String[] args)
    {
        ArrayList<Vehicule> flotte = new LectureVehicules().lireVehicules("vehicules.csv");
        ArrayList<Ligne> lignes = new LectureLignes().lire("lignes.csv", "lignes_trolleys.txt");
        System.out.println("la ligne la plus utilisée est la ligne: "+trouveLigneLaPlusUtilisee(flotte, lignes));
        //ajoutez ici l'affichage de la liste des Arrêts de la ligne la plus utilisée
        //todo
    }

    public static Ligne trouveLigneLaPlusUtilisee(ArrayList<Vehicule> flotte, ArrayList<Ligne> lignes){
        //déclarer une HashMap contenant comme clé un nom de ligne et comme valeur le nombre d'utilisation
        HashMap<String, Integer> utilisationsDesLignes = calculeUtilisations(flotte);

        //parcourir la HashMap et trouver la clé dont la valeur associée est maximale
        String ligneMax= trouveNomLigneMax(utilisationsDesLignes);

        //parcourir la liste des Lignes et retourner la ligne dont le nom correspond à la clé trouvée au-dessus (ou null si non trouvé)
        return trouveLigne(ligneMax, lignes);
    }

    public static Ligne trouveLigne(String nomLigne, ArrayList<Ligne> lignes){
        for(Ligne l : lignes){
            if(nomLigne.equals(l.getNom()))
                return l;
        }
        return null;
    }

    public static String trouveNomLigneMax(HashMap<String,Integer> map){
        int max = Integer.MIN_VALUE;
        String ligneMax = "";
        for (String s : map.keySet())
        {
            if(map.get(s) > max){
                max = map.get(s);
                ligneMax = s;
            }
        }
        return ligneMax;
    }

    public static HashMap<String,Integer> calculeUtilisations(ArrayList<Vehicule> flotte){
        HashMap<String,Integer> map = new HashMap();
        //parcourir la liste de véhicules
        for(Vehicule v : flotte) {
            //incrémenter le nombre d'utilisations de chaque ligne du vehicule dans la HashMap
            for(String nomLigne : v.getListeLignes()) {
                if(!map.containsKey(nomLigne))
                    map.put(nomLigne,1);
                else
                    map.put(nomLigne, map.get(nomLigne) + 1);
            }
        }
        return map;
    }

    // Pour les costaud(e)s: l'algorithme proposé peut être adapté (par exemple utiliser une HashMap avec des objets Ligne comme clés) => Quelles modifications envisager?
}
