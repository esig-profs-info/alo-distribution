import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * April 2020
 *
 * @author Mirko Steinle
 */
class LectureLignes
{
    ArrayList<Ligne> lire(String fichierLignes, String fichierTrolleys)
    {
        ArrayList<Ligne> listeLignes = new ArrayList<>();
        try (Scanner scanSurFichier = new Scanner(new File(fichierLignes))) {
            scanSurFichier.nextLine(); // on saute la ligne des titres

            Ligne ligneCourante = null;
            while (scanSurFichier.hasNextLine()){
                String ligne = scanSurFichier.nextLine();
                Scanner scanSurLigne = new Scanner(ligne);
                scanSurLigne.useDelimiter(";");

                String nomLigne = scanSurLigne.next();
                if(ligneCourante == null)
                { // c'est la première ligne
                    ligneCourante = new Ligne(nomLigne);
                    lireInfosTrolley(ligneCourante, fichierTrolleys);
                }
                else if (!ligneCourante.hasNom(nomLigne))
                { // une nouvelle ligne => fin construction de la précédente
                    listeLignes.add(ligneCourante);
                    ligneCourante = new Ligne(nomLigne);
                    lireInfosTrolley(ligneCourante, fichierTrolleys);
                }
                ligneCourante.ajouterMinutage(scanSurLigne.nextInt());
                ligneCourante.ajouterArret(scanSurLigne.next());
            }
            if (ligneCourante != null)
            { // la dernière ligne: il faut l'ajouter après la boucle
                listeLignes.add(ligneCourante);
            }
        }
        catch(FileNotFoundException fnfe){
            System.out.println("fichier non trouvé!");
        }
        return listeLignes;

    }

    private void lireInfosTrolley(Ligne ligneCourante, String fichierTrolleys)
    {
        try (Scanner scan = new Scanner(new File(fichierTrolleys)))
        {
            while (scan.hasNext())
            {
                Scanner sl = new Scanner(scan.nextLine());
                String nomLigne = sl.next();
                int tension = sl.nextInt();
                if (ligneCourante.hasNom(nomLigne))
                {
                    ligneCourante.setCompatibleTrolley(true);
                    ligneCourante.setTensionLigne(tension);
                    return;
                }
            }
        } catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
    }


    }
